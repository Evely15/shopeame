import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './core/footer/footer.component';
import { HeaderComponent } from './core/header/header.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { ProductsComponent } from './pages/products/products.component';
import { CardProductComponent } from './pages/products/card-product/card-product.component';
import { HttpClientModule } from '@angular/common/http';
import { ProductformComponent } from './pages/productform/productform.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NewProductFormComponent } from './pages/new-product-form/new-product-form.component';



@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    HomePageComponent,
    ProductsComponent,
    CardProductComponent,
    ProductformComponent,
    NewProductFormComponent
    
  ],
  imports: [
    HttpClientModule, 
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
