import { Component, OnInit } from '@angular/core';
import { Product } from '../../model/product.model';
import { FormBuilder, Validators } from '@angular/forms'; 
import { ActivatedRoute } from '@angular/router';
import { ProductsService} from '../../shared/products.service';




@Component({
  selector: 'app-productform',
  templateUrl: './productform.component.html',
  styleUrls: ['./productform.component.scss']
})
export class ProductformComponent implements OnInit {
  
  product: Product={
    id: 0,
    name: '',
    price: 0,
    description: '',
    stars: 0,
    image: ''
  };
  
  productId: any='';

  productForm;
  
  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private productService:ProductsService) {
    this.productForm = this.formBuilder.group({
      id:[0],
      name: ['', Validators.required],
      price: [0,[Validators.required]],
      description: ['',[Validators.required]], 
      stars: 0,
      image: ['', [Validators.required]],
    })
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.productId=params.get('productId')
      console.log(this.productId)
    });
    this.productService.getProduct(this.productId).subscribe((res:any) => { 
      this.product = res 
      console.log(res);
      this.productForm.setValue(
       { 
          id:this.product.id,
          name: this.product.name,
          price: this.product.price,
          description: this.product.description,
          stars: this.product.stars,
          image: this.product.image
      });
    });
    
    console.log(this.productForm.value)
    this.onValueChanges()
  }
  submitted(){
    console.log(this.productForm)
    console.log(this.productForm.value)
    
        console.log(this.productService.putProduct(this.productForm.value));
  }

  onValueChanges(): void {
    this.productForm.valueChanges.subscribe(val=>{
      this.product=val
    })
 
  }
}
