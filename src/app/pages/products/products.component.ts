import { Component, OnInit } from '@angular/core';
import {Product} from '../../model/product.model'
import { ProductsService } from '../../shared/products.service'
import { FormBuilder } from '@angular/forms'; 

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  
  data : Product[]=[]
  originaldata : Product[]=[]
  contactForm;
  // filter:string=''

  constructor(private formBuilder: FormBuilder, private productsService:ProductsService) {
    this.contactForm = this.formBuilder.group({
      filter: [''],
    })
  }

 

  ngOnInit(): void {
    
    this.productsService.getProducts().subscribe((res:any) => { 
      this.data = res 
      this.originaldata=res
      console.log(res);
    })
    this.onValueChanges();
  }

  submitted(){
    console.log(this.contactForm)
    console.log(this.contactForm.value)
  }

  
  onValueChanges(): void {
    this.contactForm.get('filter')?.valueChanges.subscribe(value=>{
      console.log(value.length>0)

       this.data=this.filterItems(value);
      })
  }
  
  filterItems(query:string) {
    return this.originaldata.filter(function(el:Product) {
        return el.name.toLowerCase().includes(query.toLowerCase())
    })
  }
  

}

