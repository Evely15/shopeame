import { Component, Input, OnInit } from '@angular/core';
import {Product} from '../../../model/product.model'
@Component({
  selector: 'app-card-product',
  templateUrl: './card-product.component.html',
  styleUrls: ['./card-product.component.scss']
})
export class CardProductComponent implements OnInit {

 @Input() product: Product={
  id: 0,
  name: "",
  price: 0,
  description: "",
  stars: 0,
  image: ""
};
@Input() index: number=0;



  constructor() {
    
    
   }

  ngOnInit(): void {
    
  }

}


