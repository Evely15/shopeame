import { Component, OnInit } from '@angular/core';
import { Product } from '../../model/product.model';
import { FormBuilder, Validators } from '@angular/forms'; 
// import { ActivatedRoute } from '@angular/router';
import { ProductsService} from '../../shared/products.service';


@Component({
  selector: 'app-new-product-form',
  templateUrl: '../productform/productform.component.html',
  styleUrls: ['./new-product-form.component.scss']
})
export class NewProductFormComponent implements OnInit {

  product: Product={
    id: 0,
    name: '',
    price: 0,
    description: '',
    stars: 0,
    image: ''
  };
  
  productId: any='';

  productForm;
  
  constructor(private formBuilder: FormBuilder, private productService:ProductsService) {
    this.productForm = this.formBuilder.group({
      id:[0],
      name: ['', Validators.required],
      price: [0,[Validators.required]],
      description: ['',[Validators.required]], 
      stars: 0,
      image: ['', [Validators.required]],
    })
  }

  ngOnInit(): void {
    
    this.productService.getProducts().subscribe((res:any) => { 
      this.product = res 
      console.log(res);
      let newId=res.lenght++
      this.productForm.patchValue({id:newId});
    });
    
    
    this.onValueChanges()
  }
  submitted(){
    console.log(this.productForm)
    console.log(this.productForm.value)
    
        console.log(this.productService.postProduct(this.productForm.value));
  }

  onValueChanges(): void {
    this.productForm.valueChanges.subscribe(val=>{
      console.log(val)
      this.product=val
    })
 
  }

}
