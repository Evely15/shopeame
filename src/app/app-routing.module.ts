import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './pages/home-page/home-page.component'
import { ProductsComponent } from './pages/products/products.component';
import { ProductformComponent } from './pages/productform/productform.component'
import { NewProductFormComponent } from './pages/new-product-form/new-product-form.component'

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomePageComponent },
  { path: 'products', component: ProductsComponent },
  { path: 'product/:productId', component: ProductformComponent },
  { path: 'new', component: NewProductFormComponent }
  


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
