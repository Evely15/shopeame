import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product } from '../model/product.model';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private http:HttpClient) { }

  getProducts(){
    console.log("voy a llamar a los productos")
    return this.http.get('http://localhost:3000/products')
  }
  
  getProduct(id:number){
    console.log("voy a llama 1 producto")
    return this.http.get('http://localhost:3000/products/'+id)
  }

  postProduct(product:Product){
    const headers = { 'content-type': 'application/json'}  
    const body=JSON.stringify(product);
    console.log("el body" + body)
    return this.http.post<Product>('http://localhost:3000/products',body,{'headers':headers} ).subscribe(data => {
      console.log(data);
  });
  }

  putProduct(product:Product){
    const headers = { 'content-type': 'application/json'}  
    const body=JSON.stringify(product);
    console.log("el body" + body)
    return this.http.put<Product>('http://localhost:3000/products/'+product.id, body,{'headers':headers} ).subscribe(data => {
      console.log(data);
  });
  }

  
}

